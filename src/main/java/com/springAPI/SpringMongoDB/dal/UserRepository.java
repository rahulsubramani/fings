package com.springAPI.SpringMongoDB.dal;



import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.springAPI.SpringMongoDB.model.User;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
}
